package com.anasilvia.componseapp.util

object Constants {
    const val BASE_URL = "https://tastedive.com/api/"
    const val SCREEN_MAIN = "MainScreen"
    const val SCREEN_LIST = "ListScreen?title={title}&movies={movies}&music={music}&books={books}"
}