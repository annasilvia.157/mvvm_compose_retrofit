package com.anasilvia.componseapp.api

import com.anasilvia.componseapp.data.models.MovieReponse
import retrofit2.http.*

interface MoviesApi {
    @GET("similar")
    suspend fun getMovies(
        @Query("q") query: String,
        @Query("type") type: String,
        @Query("info") info: Int
    ): MovieReponse
}