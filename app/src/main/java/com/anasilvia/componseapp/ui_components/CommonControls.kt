package com.anasilvia.componseapp.ui_components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.anasilvia.componseapp.ui.theme.ButtonColor
import com.anasilvia.componseapp.ui.theme.ButtonColor2
import com.anasilvia.componseapp.ui.theme.buttonText

@Composable
fun Button(action: () -> Unit){
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(16.dp))
            .height(50.dp)
            .clickable {
                action()
            }
            .background(
                brush = Brush.linearGradient(
                    colors = listOf(
                        MaterialTheme.colors.ButtonColor2,
                        MaterialTheme.colors.ButtonColor
                    )
                )
            ),
    ) {
        Text(
            text = "Buscar",
            modifier = Modifier
                .align(Alignment.Center),
            color = MaterialTheme.colors.buttonText,
            fontWeight = FontWeight.Bold
        )
    }
}
