package com.anasilvia.componseapp.ui.screens.ui_utils

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role.Companion.Image
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.anasilvia.componseapp.R
import com.anasilvia.componseapp.ui.theme.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import com.anasilvia.componseapp.ui_components.ralewayFamily
import com.anasilvia.componseapp.util.Action

@Composable
fun ListAppBar(onSearchClicked: () -> Unit,title:String){
    DefaultAppBar(
        title
    )
}

@Composable
fun DefaultAppBar(
    text:String
){
    TopAppBar(
        title = {
            Text(
                text = text,
                color = MaterialTheme.colors.topAppBarContentColor,
                fontFamily = ralewayFamily, fontWeight = FontWeight.Bold
            )
        },
        backgroundColor = MaterialTheme.colors.topAppBarBackgroundColor,
        elevation = 0.2.dp
    )
}

@Composable
fun SearchAppBar(
    onSearchClicked: () -> Unit,
    text:String
){
    TopAppBar(
        title = {
            Text(
                text = text,
                color = MaterialTheme.colors.topAppBarContentColor,
                fontFamily = ralewayFamily, fontWeight = FontWeight.Bold
            )
        },
        backgroundColor = MaterialTheme.colors.topAppBarBackgroundColor,
        elevation = 0.2.dp,
        actions = {
            SearchAction(onSearchClicked)
        }
    )
}

@Composable
fun SearchAction(
    onSearchClicked: () -> Unit
){
    IconButton(onClick = { onSearchClicked() }) {
        Icon(
            imageVector = Icons.Filled.Search,
            contentDescription = stringResource(R.string.search_tasks),
            tint = MaterialTheme.colors.topAppBarContentColor
        )
    }
}

@Composable
fun DetailsAppBar(
    navigateToTaskScreen: (Action) -> Unit = {}
){
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(140.dp),
    ){
            Image(
                modifier = Modifier.fillMaxWidth(),
                painter = painterResource(id = R.drawable.schools),
                contentDescription = "",
                contentScale = ContentScale.FillWidth
            )

        Box(
            contentAlignment = Alignment.BottomStart,
            modifier = Modifier
                .padding(18.dp)
        ){
            Icon(
                modifier= Modifier
                    .clickable {
                        navigateToTaskScreen(Action.UPDATE)
                    }
                ,
                painter = painterResource(id = R.drawable.ic_arrow_back),
                tint = Color.Black,
                contentDescription = null
            )
        }
    }
}

@Composable
@Preview
fun DefaultBarApp2(){
    DetailsAppBar()
}

@Composable
@Preview
fun PreviewAppBar(){
    DefaultAppBar(
        "Movies"
    )
}

