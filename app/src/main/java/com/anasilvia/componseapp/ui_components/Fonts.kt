package com.anasilvia.componseapp.ui_components

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import com.anasilvia.componseapp.R

val ralewayFamily = FontFamily(
    Font(R.font.raleway_normal, FontWeight.Normal),
    Font(R.font.raleway_bold, FontWeight.Bold),
    Font(R.font.raleway_medium, FontWeight.Medium),
)