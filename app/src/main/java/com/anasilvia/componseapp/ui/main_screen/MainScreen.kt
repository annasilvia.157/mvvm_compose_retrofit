package com.anasilvia.componseapp.ui.main_screen

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.anasilvia.componseapp.data.models.MoviesListContract
import com.anasilvia.componseapp.ui.screens.ui_utils.DefaultAppBar
import com.anasilvia.componseapp.ui.theme.*
import com.anasilvia.componseapp.ui_components.Button

@Composable
fun InitialScreen(
    goListScreen: () -> Unit,
    mainViewModel: MainViewModel = viewModel()
){
    Scaffold(
        topBar = {
            DefaultAppBar(
                "Búsqueda"
            )
        },
        content = {
            ScreenContent(goListScreen,mainViewModel)
        },
    )
}

@Composable
fun ScreenContent(
    goListScreen: () -> Unit,
    mainViewModel: MainViewModel = viewModel(),
){
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .background(
                brush = Brush.linearGradient(
                    colors = listOf(
                        MaterialTheme.colors.backgroundScreen,
                        MaterialTheme.colors.backgroundScreen2
                    )
                )
            ),
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 16.dp)
        ) {

            Text(
                text = "Seleccione el tipo",
                fontWeight = FontWeight.Medium,
                color = MaterialTheme.colors.subtitles
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            )
            {
                LabelledCheckbox(
                    checked = mainViewModel.state.movie,
                    onCheckedChange = { mainViewModel.enableMovie(it) },
                    label = "Movies"
                )
                LabelledCheckbox(
                    checked = mainViewModel.state.book,
                    onCheckedChange = { mainViewModel.enableBook(it) },
                    label = "Books"
                )
                LabelledCheckbox(
                    checked = mainViewModel.state.music,
                    onCheckedChange = { mainViewModel.enableMusic(it)  },
                    label = "Musica"
                )
            }

            Spacer(Modifier.width(36.dp))

            OutlinedTextField(
                modifier = Modifier
                    .fillMaxWidth(),
                value = mainViewModel.state.query,
                onValueChange = { item ->
                    mainViewModel.updateQuery(item)
                },

                label = {
                    Text("Ingresa el titulo")
                },
                colors = TextFieldDefaults.outlinedTextFieldColors
                (
                    unfocusedBorderColor = MaterialTheme.colors.labelDisabled,
                    focusedBorderColor  = MaterialTheme.colors.labelColor,
                    focusedLabelColor = MaterialTheme.colors.labelColor
                )
            )

            Spacer(Modifier.height(36.dp))

            Button(goListScreen)
        }
    }
}

@Composable
fun LabelledCheckbox(
    checked: Boolean,
    onCheckedChange: (Boolean) -> Unit,
    label: String,
) {
    Row(
        modifier = Modifier
            .height(48.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {

        Checkbox(
            checked = checked,
            onCheckedChange = onCheckedChange
        )
        Spacer(Modifier.width(12.dp))
        Text(label)
    }
}

@Preview(
    showSystemUi = true,
    name = "Light Mode",
    showBackground = true)
@Preview(
    name = "Dark Mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    showSystemUi = true
)
@Composable
fun ListLigthPreview(){
    ComponseAppTheme{
    ScreenContent(
        {}
    )
    }
}