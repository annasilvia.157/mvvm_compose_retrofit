package com.anasilvia.componseapp.ui.main_screen

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    var state by mutableStateOf(
        MainControls.State(
            movie = false,
            book = false,
            music = false,
            query= "",
            type = ""
        )
    )

    fun enableMovie(checked:Boolean){
        state = state.copy(movie = checked)
    }
    fun enableBook(checked:Boolean){
        state = state.copy(book = checked)
    }
    fun enableMusic(checked:Boolean){
        state = state.copy(music = checked)
    }
    fun updateQuery(query:String){
        state = state.copy(query = query)
    }
}

class MainControls {
    data class State(
        val movie:Boolean,
        val book:Boolean,
        val music:Boolean,
        val query:String,
        val type:String
    )
}