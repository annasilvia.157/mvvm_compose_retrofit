package com.anasilvia.componseapp.ui.list_items

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.lifecycle.ViewModelProvider
import com.anasilvia.componseapp.data.repositories.MoviesRepository
import com.anasilvia.componseapp.ui.theme.ComponseAppTheme


class ListActivity: ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val type = intent.getStringExtra("type")
        val title = intent.getStringExtra("title")

        setContent {
            ComponseAppTheme {
                val repository = MoviesRepository()
                val viewModelFactory = ListViewModelFactory(repository,title!!,type!!)
                val listViewModel = ViewModelProvider(this@ListActivity,viewModelFactory)[ListViewModel::class.java]

                ListScreen(
                    goToWebPage = {
                        goToUrl(it)
                    },
                    viewModel = listViewModel
                )
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        super.onDestroy()
    }

    private fun goToUrl(url:String){
        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(url)
            )
        )
    }

}