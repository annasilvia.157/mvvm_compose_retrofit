package com.anasilvia.componseapp.ui.list_items

import androidx.compose.runtime.*
import androidx.lifecycle.*
import com.anasilvia.componseapp.data.models.MoviesListContract
import com.anasilvia.componseapp.data.repositories.MoviesRepository
import kotlinx.coroutines.launch

class ListViewModel(
    private val repository: MoviesRepository,
    private val title:String,
    private val type:String
) : ViewModel(){

    var state by mutableStateOf(
        MoviesListContract.State(
            results = listOf(),
            isLoading = true,
            isExpanded = false
        )
    )

    var errorMessage by mutableStateOf("")

    var titleScreen = "Resultados"

    init {
        searchMovies()
    }

    private fun searchMovies(){
        viewModelScope.launch {
            getMovieList()
        }
    }

    private suspend fun getMovieList() {
        val results = repository.getMovies(title,type)
        viewModelScope.launch {
            state = state.copy(results = results, isLoading = false)
            if(results.isEmpty()){
                errorMessage = "No se encontraron resultados"
            }
        }
    }

    fun isExpanded(expanded:Boolean){
        state = state.copy(isExpanded = expanded)
    }
}
