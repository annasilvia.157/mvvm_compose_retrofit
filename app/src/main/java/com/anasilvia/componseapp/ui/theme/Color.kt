package com.anasilvia.componseapp.ui.theme

import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val Purple700 = Color(0xFF3700B3)
val Purple500 = Color(0xFF7A61B3)

val AccentLightColor = Color(0xFF06DDDD)
val AccentDarkColor = Color(0xFFEAAD03)
val Orange = Color(0xFFFF9B38)
val OrangeLigth = Color(0xFFF7D4B1)
val Yellow = Color(0xFFFDC63D)
val YellowLigth = Color(0xFFF7CD7F)
val white = Color(0xFFF4F7F7)

val CardsGray = Color(0xFF2A2D44)
val GrayMedium = Color(0xFF494B5A)
val Screens = Color(0xFF171724)
val Screens2 = Color(0xFF2E2E38)
val barColor = Color(0xD6DEDF)

val ScreensLigth = Color(0xFFF5F5F7)
val ScreensLigth2 = Color(0xFFFDFDFD)

val Colors.topAppBarContentColor: Color
@Composable
get() = if(isLight) CardsGray else white

val Colors.backgrondColor: Color
    @Composable
    get() = if(isLight) white else Screens

val Colors.topAppBarBackgroundColor: Color
    @Composable
    get() =   if(isLight) barColor else Screens

val Colors.GradientStart: Color
    @Composable
    get() = if(isLight) white else Screens

val Colors.GradientEnd: Color
    @Composable
    get() = if(isLight) white else Screens2

val Colors.AccentText: Color
    @Composable
    get() = if(isLight) AccentLightColor else AccentDarkColor

val Colors.AccentColor: Color
    @Composable
    get() = if(isLight) Purple700 else Yellow

val Colors.ButtonColor: Color
    @Composable
    get() = if(isLight) Purple500 else YellowLigth

val Colors.ButtonColor2: Color
    @Composable
    get() = if(isLight) Purple700 else Orange

val Colors.buttonText: Color
    @Composable
    get() =  white

val Colors.backgroundScreen: Color
    @Composable
    get() = if(isLight) ScreensLigth else Screens

val Colors.backgroundScreen2: Color
    @Composable
    get() = if(isLight) ScreensLigth2 else Screens2

val Colors.subtitles: Color
    @Composable
    get() = if(isLight) GrayMedium else OrangeLigth

val Colors.labelColor: Color
    @Composable
    get() = if(isLight) Purple500 else Orange

val Colors.labelDisabled: Color
    @Composable
    get() = if(isLight) CardsGray else white