package com.anasilvia.componseapp.ui.list_items

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.anasilvia.componseapp.data.repositories.MoviesRepository

class ListViewModelFactory(private val repository: MoviesRepository,private val title:String,private val type: String): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ListViewModel(repository,title,type) as T
    }
}