package com.anasilvia.componseapp.ui.list_items

import android.content.res.Configuration
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.anasilvia.componseapp.R
import com.anasilvia.componseapp.data.models.Movie
import com.anasilvia.componseapp.ui.screens.ui_utils.DefaultAppBar
import com.anasilvia.componseapp.ui.theme.*
import com.anasilvia.componseapp.ui_components.ralewayFamily

@Composable
fun ListScreen(
    goToWebPage: (url: String) -> Unit,
    viewModel: ListViewModel = viewModel()
){
    ScreenContent(
        goToWebPage = goToWebPage
    )

    if (viewModel.state.isLoading)
        LoadingBar()
}

@Composable
fun LoadingBar() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun ScreenContent(
    goToWebPage: (id: String) -> Unit = { },
    viewModel: ListViewModel = viewModel()
){
    Scaffold(
        topBar = {
            DefaultAppBar(viewModel.titleScreen)
        },
        content = {
            MessageList(viewModel.state.results, viewModel.errorMessage, viewModel, goToWebPage)
        },
        backgroundColor = MaterialTheme.colors.backgrondColor
    )
}

@Composable
fun MessageList(
    movies: List<Movie>,
    errorMessage: String,
    viewModel: ListViewModel = viewModel(),
    goToWebPage: (url: String) -> Unit = { },
) {
    if(movies.isNotEmpty()) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .background(
                    brush = Brush.linearGradient(
                        colors = listOf(
                            MaterialTheme.colors.backgroundScreen,
                            MaterialTheme.colors.backgroundScreen,
                            MaterialTheme.colors.backgroundScreen2
                        )
                    )
                ),
        ) {

        Column {
            Text(
                text = "Registros de hoy",
                modifier = Modifier.padding(
                    start = 16.dp,
                    top = 16.dp
                ),
                color = MaterialTheme.colors.AccentText,
                fontFamily = ralewayFamily, fontWeight = FontWeight.Bold)

            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth(),
                contentPadding = PaddingValues(16.dp)
            ) {
                items(movies) { movies ->
                    MessageRow(movies, viewModel, goToWebPage)
                }
            }
        }
        }
    }
    else{
        Text(
            text = errorMessage,
            modifier = Modifier.padding(
                start = 16.dp,
                top = 16.dp
            ),
            color = MaterialTheme.colors.AccentText,
            fontFamily = ralewayFamily, fontWeight = FontWeight.Bold
        )
    }
}

@Composable
fun MessageRow(
    school: Movie,
    viewModel: ListViewModel = viewModel(),
    goToWebPage: (url: String) -> Unit = { },
){

    Surface(
        shape = RoundedCornerShape(16.dp),
        elevation = 8.dp,
        modifier = Modifier.padding(vertical = 8.dp)

    ){
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .clickable {
                viewModel.isExpanded(!viewModel.state.isExpanded)
            }
            .background(
                brush = Brush.linearGradient(
                    colors = listOf(
                        MaterialTheme.colors.GradientStart,
                        MaterialTheme.colors.GradientEnd
                    )
                )
            )
    ){
        Column(
            modifier = Modifier
                .padding(10.dp)
                .fillMaxWidth()
        ) {
            Row(
                modifier = Modifier
                .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {

                Image(
                    painter = painterResource(id = R.drawable.movie_image),
                    contentDescription = "Movie Image")

                Column(
                    Modifier
                        .padding(
                            start = 18.dp,
                            top = 8.dp
                        )
                ) {
                    Text(
                        text = school.name,
                        style = MyTypography.subtitle1,
                        color = MaterialTheme.colors.onSurface,
                    )
                    Text(
                        text = "Tipo: ${school.type}",
                        style = MaterialTheme.typography.body2,
                        color = MaterialTheme.colors.onSurface,
                    )
                    Text(
                        text = "",
                        style = MaterialTheme.typography.body2,
                    )
                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.End,
                        verticalAlignment = Alignment.CenterVertically
                    ){

                        if(school.yUrl != null){
                        Icon(
                            painter = painterResource(id = R.drawable.ic_youtube),
                            tint = MaterialTheme.colors.AccentColor,
                            contentDescription = null,
                            modifier = Modifier
                                .padding(end = 5.dp)
                                .clickable { goToWebPage(school.yUrl) }
                        )
                        }
                        if(school.wUrl!= null){
                        Icon(
                            painter = painterResource(id = R.drawable.ic_web),
                            tint = MaterialTheme.colors.AccentColor,
                            contentDescription = null,
                            modifier = Modifier
                                .padding(end = 5.dp)
                                .clickable { goToWebPage(school.wUrl) }
                        )
                        }
                    }
                }
            }

            AnimatedVisibility(visible = viewModel.state.isExpanded) {
                Text(
                    text = school.wTeaser!!,
                    color = MaterialTheme.colors.onSurface,
                )
            }
        }
    }
    }
}

@Preview(
    showSystemUi = true,
    name = "Light Mode",
    showBackground = true)
@Preview(
    name = "Dark Mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    showSystemUi = true
)
@Composable
fun ListPreview(){
    ComponseAppTheme {
        ScreenContent(
            {},
        )
    }
}