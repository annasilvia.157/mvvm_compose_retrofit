package com.anasilvia.componseapp.ui.main_screen

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import com.anasilvia.componseapp.ui.list_items.ListActivity
import com.anasilvia.componseapp.ui.theme.ComponseAppTheme
import dagger.hilt.android.AndroidEntryPoint
/*
* Created by: ANA CARRILLO 25-MAY-22 */
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ComponseAppTheme {
                        val mainViewModel: MainViewModel by viewModels()

                        InitialScreen(
                            goListScreen =  {
                                var searchType = ""

                                if(mainViewModel.state.music){
                                    searchType = "music,"
                                }
                                if(mainViewModel.state.movie){
                                    searchType += "movie,"
                                }
                                if(mainViewModel.state.book){
                                    searchType += "book"
                                }
                                goToListScreen(mainViewModel.state.query,searchType)
                            }
                            ,
                            mainViewModel
                        )
                }
            }
        }

    private fun goToListScreen(title:String, type:String){
        val intent = Intent(this@MainActivity, ListActivity::class.java)
        intent.putExtra("title", title)
        intent.putExtra("type", type)
        startActivity(intent)
    }
}
