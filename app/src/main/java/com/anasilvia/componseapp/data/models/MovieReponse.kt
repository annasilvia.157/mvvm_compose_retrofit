package com.anasilvia.componseapp.data.models

import com.squareup.moshi.Json

data class MovieReponse(
    @Json(name = "Similar")
    val similar: Result,
)

class Result(
    @Json(name = "Results")
    val list: List<Movie>
    )