package com.anasilvia.componseapp.data.models

import com.squareup.moshi.Json

data class Movie(
    @Json(name = "Name")
    val name: String,

    @Json(name = "wTeaser")
    val wTeaser: String?,

    @Json(name = "wUrl")
    val wUrl: String?,

    @Json(name = "yUrl")
    val yUrl: String?,

    @Json(name = "Type")
    val type:String)