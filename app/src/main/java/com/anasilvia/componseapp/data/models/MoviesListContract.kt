package com.anasilvia.componseapp.data.models

class MoviesListContract {
    data class State(
        val results: List<Movie> = listOf(),
        val isLoading: Boolean = false,
        val isExpanded: Boolean = false
    )
}