package com.anasilvia.componseapp.data.repositories

import com.anasilvia.componseapp.api.RetrofitInstance
import com.anasilvia.componseapp.data.models.Movie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MoviesRepository {

    suspend fun getMovies(query:String,type:String): List<Movie> = withContext(Dispatchers.IO) {

        val movies = RetrofitInstance.api.getMovies(query,type,1 )

        return@withContext movies.similar.list
    }
}